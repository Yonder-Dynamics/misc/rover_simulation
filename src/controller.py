#!/usr/bin/env python3

import curses
import time
import threading

class Controller(threading.Thread):
    def __init__(self, rate=50, speed_gain=1, omega_gain=1, speed_cap=5, omega_cap=5):
        super().__init__()
        print("press q to quit")

        self.speed_gain = speed_gain
        self.speed_cap = speed_cap
        self.speed = 0
        
        self.omega_gain = omega_gain
        self.omega_cap = omega_cap
        self.omega = 0

        self.done = False
        self.sleep_time = 1.0/rate

        self.stdscr = curses.initscr()
        self.stdscr.keypad(True)
        self.stdscr.scrollok(True)
        curses.noecho()
        curses.cbreak() # no enter key needed

    def on_done(self):
        self.stdscr.keypad(False)
        curses.nocbreak()
        curses.echo()
        curses.endwin()
        self.done = True

    def change_speed(self, increment):
        if self.speed/increment < 0: # different sign
            self.speed = 0
        else: self.speed += increment
        if self.speed > self.speed_cap: self.speed = self.speed_cap
        elif self.speed < -self.speed_cap: self.speed = -self.speed_cap

    def change_omega(self, increment):
        if self.omega/increment < 0: # different sign
            self.omega = 0
        else: self.omega += increment
        if self.omega > self.omega_cap: self.omega = self.omega_cap
        elif self.omega < -self.omega_cap: self.omega = -self.omega_cap

    def run(self):
        speed_increment = self.speed_gain*self.sleep_time
        omega_increment = self.omega_gain*self.sleep_time
        while True:
            time.sleep(self.sleep_time)

            c = self.stdscr.getch()
            if c == ord('q') or c == ord('c'):
                break
            elif c == ord('w'):
                self.change_speed(speed_increment)
            elif c == ord('a'):
                self.change_omega(-omega_increment)
            elif c == ord('s'):
                self.change_speed(-speed_increment)
            elif c == ord('d'):
                self.change_omega(omega_increment)
        self.on_done()

    def get_speed(self):
        return self.speed

    def get_omega(self):
        return self.omega

    def is_done(self):
        return self.done

if __name__ == "__main__":
    controller = Controller()
    controller.start()

    while not controller.is_done():
        time.sleep(0.5)        
        print(controller.get_speed(), controller.get_omega())
