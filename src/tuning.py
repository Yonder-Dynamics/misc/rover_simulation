#!/usr/bin/env python3

import rospy
from std_msgs.msg import Float64MultiArray
from tkinter import *

master = Tk()

upDown_gravity = Scale(master, from_=0, to=10, label="upDown_gravity", orient=HORIZONTAL, resolution=0.001, length=500)
elbow_gravity  = Scale(master, from_=0, to=10, label="elbow_gravity",  orient=HORIZONTAL, resolution=0.001, length=500)

swivel_speed_p = Scale(master, from_=0, to=100, label="swivel_speed_k", orient=HORIZONTAL, resolution=0.01, length=500)
swivel_speed_d = Scale(master, from_=0, to=30, label="swivel_speed_d", orient=HORIZONTAL, resolution=0.01, length=500)

upDown_speed_p = Scale(master, from_=0, to=100, label="upDown_speed_k", orient=HORIZONTAL, resolution=0.01, length=500)
upDown_speed_d = Scale(master, from_=0, to=40, label="upDown_speed_d", orient=HORIZONTAL, resolution=0.01, length=500)

elbow_speed_p  = Scale(master, from_=0, to=100, label="elbow_speed_k",  orient=HORIZONTAL, resolution=0.01, length=500)
elbow_speed_d  = Scale(master, from_=0, to=30, label="elbow_speed_d",  orient=HORIZONTAL, resolution=0.01, length=500)

swivel_p = Scale(master, from_=0, to=400, label="swivel_p", orient=HORIZONTAL, resolution=0.01, length=500)
swivel_d = Scale(master, from_=0, to=50,  label="swivel_d", orient=HORIZONTAL, resolution=0.01, length=500)
 
upDown_p = Scale(master, from_=0, to=1000, label="upDown_p", orient=HORIZONTAL, resolution=0.01, length=500)
upDown_d = Scale(master, from_=0, to=50,   label="upDown_d", orient=HORIZONTAL, resolution=0.01, length=500)
 
elbow_p =  Scale(master, from_=0, to=1000, label="elbow_p",  orient=HORIZONTAL, resolution=0.01, length=500)
elbow_d =  Scale(master, from_=0, to=50,   label="elbow_d",  orient=HORIZONTAL, resolution=0.01, length=500)

upDown_gravity.pack()
elbow_gravity.pack()

swivel_speed_p.pack()
swivel_speed_d.pack()

upDown_speed_p.pack()
upDown_speed_d.pack()

elbow_speed_p.pack() 
elbow_speed_d.pack() 

swivel_p.pack()
swivel_d.pack()

upDown_p.pack()
upDown_d.pack()

elbow_p.pack()
elbow_d.pack()

def send():
    pub = rospy.Publisher("/rover_simulation/tuning_params", Float64MultiArray, queue_size=10)

    consts = Float64MultiArray()
    consts.data = [0]*14 # 14 constants
    # [swivel_speed, upDown_speed, elbow_speed, [swivel_pid], [upDown_pid], [elbow_pid]]

    while not rospy.is_shutdown():
        master.update()

        consts.data[0 ] = upDown_gravity.get()
        consts.data[1 ] = elbow_gravity.get()
        consts.data[2 ] = swivel_speed_p.get()
        consts.data[3 ] = swivel_speed_d.get()
        consts.data[4 ] = upDown_speed_p.get()
        consts.data[5 ] = upDown_speed_d.get()
        consts.data[6 ] =  elbow_speed_p.get()
        consts.data[7 ] =  elbow_speed_d.get()
        consts.data[8 ] = swivel_p.get()
        consts.data[9 ] = swivel_d.get()
        consts.data[10] = upDown_p.get()
        consts.data[11] = upDown_d.get()
        consts.data[12] =  elbow_p.get()
        consts.data[13] =  elbow_d.get()

        pub.publish(consts)
        rate.sleep()

if __name__ == '__main__':
    #print("press q to quit")
    rospy.init_node('consts_sender', anonymous=True)
    rate = rospy.Rate(50) # 50hz
    try:
        send()
    except rospy.ROSInterruptException:
        pass
