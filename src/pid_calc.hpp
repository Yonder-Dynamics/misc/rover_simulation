#include <iostream>
#include <chrono>

using namespace std;
using namespace std::chrono;

const static float defaultKP = 7;
const static float defaultKI = 1;
const static float defaultKD = 0.01;
const static float defaultICap = 1;
const static float defaultCap = 10;

class PID_Controller{
private:
    float kP = defaultKP, kI = defaultKI, kD = defaultKD;
    float I = 0, iCap = defaultICap;
    float cap = defaultCap;
    float curErr = 0, lastErr = 0;
    float dt = 0, errPrime = 0;
    high_resolution_clock::time_point lastTime = high_resolution_clock::now();
public:
    PID_Controller() = default;
    PID_Controller(float _kP, float _kI, float _kD);
    PID_Controller(float _kP, float _kI, float _kD, float _iCap, float _cap);
    void setKs(float _kP, float _kI, float _kD);
    void setICap(float _iCap);
    void setCap(float _cap);
    float getPID(float error);
    float getPIDWithFF(float error, float speed);
};

PID_Controller::PID_Controller(float _kP, float _kI, float _kD) {
    setKs(_kP, kI, kD);
}

PID_Controller::PID_Controller(float _kP, float _kI, float _kD, float _iCap, float _cap) {
    setKs(_kP, kI, kD);
    setICap(_iCap);
    setCap(_cap);
}

void PID_Controller::setKs(float _kP, float _kI, float _kD) {
    kP = _kP;
    kI = _kI;
    kD = _kD;
}

void PID_Controller::setICap(float _iCap) {
    iCap = _iCap;
}

void PID_Controller::setCap(float _cap) {
    cap = _cap;
}

float PID_Controller::getPID(float error) {
    high_resolution_clock::time_point curTime = high_resolution_clock::now();
    duration<double> time_span = curTime - lastTime;
    dt = time_span.count();
    lastTime = curTime;

    lastErr = curErr;
    curErr = error;
    errPrime = (curErr-lastErr)/dt;
    
    if (curErr*lastErr == -1) { // reset I when error switches sign
        I = 0;
    }

    float P = kP*curErr;
    I += kI*curErr*dt;
    float D = kD*errPrime;

    I = min(I, iCap); // upper bound
    I = max(I, -iCap); // lower bound

    float PID = P+I+D;
    PID = min(PID, cap);
    PID = max(PID, -cap);

    return PID;
}

// test client
/*int main() {
    PID_Controller pid;
    for (int i = -1000; i < 0; i++) {
        cout << pid.getPID(((float)i)/1000.0) << endl;
    }
}*/
