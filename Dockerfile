FROM nvidia/cuda:10.1-devel-ubuntu18.04

# setup timezone
RUN echo 'Etc/UTC' > /etc/timezone && \
    ln -s /usr/share/zoneinfo/Etc/UTC /etc/localtime && \
    apt-get update && \
    apt-get install -q -y --no-install-recommends tzdata && \
    rm -rf /var/lib/apt/lists/*

# install packages
RUN apt-get update && apt-get install -q -y --no-install-recommends \
    dirmngr \
    gnupg2 \
    && rm -rf /var/lib/apt/lists/*

# setup keys
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654

# setup sources.list
RUN echo "deb http://packages.ros.org/ros/ubuntu bionic main" > /etc/apt/sources.list.d/ros1-latest.list

# setup environment
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

ENV ROS_DISTRO melodic

# label ros packages
LABEL sha256.ros-melodic-ros-core=ca489b24786862a0b7231f6181e2fb27d3794832b0086e84fa2885369e3f40b8

# install ros packages
RUN apt-get update && apt-get install -y --no-install-recommends \
    ros-melodic-ros-core=1.4.1-0* \
    && rm -rf /var/lib/apt/lists/*

ENTRYPOINT ["/ros_entrypoint.sh"]

# label ros packages
LABEL sha256.ros-melodic-desktop=48639769187c88a5c65950bd48a7f771680526dec5d36c7c32e26d546c54f274

# install ros packages
RUN apt-get update && apt-get install -y --no-install-recommends \
    ros-melodic-desktop=1.4.1-0* \
    && rm -rf /var/lib/apt/lists/*

LABEL maintainer "Alexander Mai alexandertmai@gmail.com"

RUN apt-get update \
    && apt-get install -y --no-install-recommends ros-melodic-gazebo-ros-control \
       ros-melodic-effort-controllers ros-melodic-ecl ros-melodic-catkin \
       python-catkin-tools ros-melodic-vision-opencv ros-melodic-cv-bridge ros-melodic-image-transport \
       ros-melodic-turtlesim ros-melodic-rosmsg ros-melodic-joint-trajectory-controller

RUN mkdir -p /root/catkin_ws/src
ADD . /root/catkin_ws/src/rover_simulation
WORKDIR /root/catkin_ws
SHELL ["/bin/bash", "-c"]
RUN source /opt/ros/melodic/setup.bash \
 && catkin init && catkin build \
 && echo source /opt/ros/melodic/setup.bash >> /root/.bashrc \
 && echo source /root/catkin_ws/devel/setup.bash >> /root/.bashrc
