#! /usr/bin/env python3
from nav_msgs.msg import Odometry
from std_msgs.msg import Header, Float64
from gazebo_msgs.srv import GetModelState, GetModelStateRequest
import rospy
import math

def quaternion_to_yaw(quaternion):
    x = quaternion.x
    y = quaternion.y
    z = quaternion.z
    w = quaternion.w
    siny_cosp = 2 * (w * z + x * y)
    cosy_cosp = 1 - 2 * (y * y + z * z)
    return math.atan2(siny_cosp, cosy_cosp)

rospy.init_node("odom_pub")

odom_pub = rospy.Publisher("/rover_odom", Odometry, queue_size=1)
heading_pub = rospy.Publisher("/rover_simulation/compass_hdg", Float64, queue_size=1)

rospy.wait_for_service("/gazebo/get_model_state")
get_model_srv = rospy.ServiceProxy("/gazebo/get_model_state", GetModelState)

odom = Odometry()
header = Header()
header.frame_id = "/odom"

model = GetModelStateRequest()
model.model_name = "robot"

r = rospy.Rate(30)

while not rospy.is_shutdown():
    result = get_model_srv(model)

    odom.pose.pose = result.pose
    odom.twist.twist = result.twist

    header.stamp = rospy.Time.now()
    odom.header = header

    odom_pub.publish(odom)

    heading = Float64()
    heading.data = -quaternion_to_yaw(result.pose.orientation)/math.pi*180+90
    heading_pub.publish(heading)

    r.sleep()