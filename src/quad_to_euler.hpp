#include <iostream>
#include <Eigen/Geometry>
#include <vector>

using namespace std;
using namespace Eigen;

vector<float> Quad_to_Euler(vector<float> quad) {
    Quaternion<float, 0> q(quad[0], quad[1], quad[2], quad[3]);
    auto e = q.toRotationMatrix().eulerAngles(0, 1, 2);
    vector<float> euler;
    for (int i = 0; i < 3; i++) {
        euler.push_back(e[i]);
    }
    return euler;
}

// test client
/*int main() {
    vector<float> data (4, 1);
    Quad_to_Euler(data);
}*/
