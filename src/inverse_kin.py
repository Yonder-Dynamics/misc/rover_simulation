#!/usr/bin/env python3

import rospy
from std_msgs.msg import Float64MultiArray
from geometry_msgs.msg import Point
from visualization_msgs.msg import Marker
from sensor_msgs.msg import JointState
from solver import Inv_kin
import time
import math
import numpy as np


class Inverse_kin_wrapper:
    def __init__(self):
        self.last_omega = np.zeros(3)  # angular speed
        self.des_vel = np.zeros(3)
        self.des_pt = np.array([0.8, 0, 0])  # declare point
        self.des_hand = np.zeros(3)
        self.curSwivel, self.curUpdown, self.curElbow = [0.0] * 3
        self.last_time = time.time()
        self.solver = Inv_kin()

        rospy.init_node("control_sender", anonymous=True)
        self.rate = rospy.Rate(100)  # 100hz

        joint_pos_sub = rospy.Subscriber(
            "/joint_states", JointState, self.angle_feedback
        )
        des_point_sub = rospy.Subscriber(
            "/arm_position", Float64MultiArray, self.update_des_pt
        )
        self.ang_pub = rospy.Publisher(
            "/rover_simulation/cmd_ang", Float64MultiArray, queue_size=1
        )
        self.des_pt_pub = rospy.Publisher("/visualize/des_pt", Marker, queue_size=1)
        self.cur_arm_pub = rospy.Publisher("/visualize/cur_arm", Marker, queue_size=1)

    def update_des_pt(self, msg):
        """[desX_speed, desY_speed, desZ_speed, des_wrist...]"""
        cur_time = time.time()
        dt = cur_time - self.last_time
        self.last_time = cur_time
        self.des_vel = np.array(msg.data[:3])  # get new desired velocity
        self.checkVelBound(self.des_vel)
        self.des_pt += dt * self.des_vel  # add the speed to get point
        self.checkPointBound(self.des_pt)
        self.des_hand += dt * np.array(msg.data[3:])
        # print(self.des_pt)
        des_marker = Marker()
        des_marker.header.stamp = rospy.Time.now()
        des_marker.header.frame_id = "/map"
        des_marker.type = Marker.POINTS
        end = Point()
        end.x = self.des_pt[0]
        end.y = self.des_pt[1]
        end.z = self.des_pt[2]
        des_marker.points.append(end)
        des_marker.scale.x = 0.05
        des_marker.scale.y = 0.05
        des_marker.scale.z = 0.05
        des_marker.color.a = 1.0
        des_marker.color.r = 0.0
        des_marker.color.g = 1.0
        des_marker.color.b = 0.0
        self.des_pt_pub.publish(des_marker)

    def angle_feedback(self, msg):
        """[elbow, lb, lf, lm, pinchy_left, pinchy_right, rb, rf, rm, shoulder_swivel, shoulder_updown,
  wrist_swivel, wrist_updown]"""
        self.curSwivel = msg.position[9]
        self.curUpdown = -msg.position[10]
        self.curElbow = msg.position[0]

        ###### patchy code to display the animation ######
        p_final = self.solver.calc_cartesian([self.curSwivel, self.curUpdown, self.curElbow])
        p_final_msg = Point()
        p_final_msg.x = p_final[0]
        p_final_msg.y = p_final[1]
        p_final_msg.z = p_final[2]
        # print([self.curSwivel, self.curUpdown, self.curElbow], p_final)

        xy_mag = 0.409575*math.cos(self.curUpdown)
        x = xy_mag*math.cos(self.curSwivel)
        y = xy_mag*math.sin(self.curSwivel)
        z = 0.409575*math.sin(self.curUpdown)
        p_mid_msg = Point()
        p_mid_msg.x = x
        p_mid_msg.y = y
        p_mid_msg.z = z

        p_hand_msg = Point()
        p_hand_msg.x = p_final[0]+0.2*math.cos(self.curSwivel)
        p_hand_msg.y = p_final[1]+0.2*math.sin(self.curSwivel)
        p_hand_msg.z = p_final[2]

        arm_marker = Marker()
        arm_marker.header.stamp = rospy.Time.now()
        arm_marker.header.frame_id = "/map"
        arm_marker.type = Marker.LINE_STRIP
        p_start_msg = Point()
        arm_marker.points.append(p_start_msg)
        arm_marker.points.append(p_mid_msg)
        arm_marker.points.append(p_final_msg)
        arm_marker.points.append(p_hand_msg)
        arm_marker.scale.x = 0.05
        arm_marker.scale.y = 0.05
        arm_marker.scale.z = 0.05
        arm_marker.color.a = 1.0
        arm_marker.color.r = 1.0
        arm_marker.color.g = 0.0
        arm_marker.color.b = 0.0
        self.cur_arm_pub.publish(arm_marker)

    def checkVelBound(self, des_vel):
        des_vel[0] = max(-0.33, min(des_vel[0], 0.33))
        des_vel[1] = max(-0.33, min(des_vel[1], 0.33))
        des_vel[2] = max(-0.33, min(des_vel[2], 0.33))

    def checkPointBound(self, des_pt):
        des_pt[0] = max(0, min(des_pt[0], 0.8))
        des_pt[1] = max(-0.65, min(des_pt[1], 0.65))
        des_pt[2] = max(-0.8, min(des_pt[2], 0.8))

    def send(self):
        msg = Float64MultiArray()
        msg.data = [0] * 9
        while not rospy.is_shutdown():
            #print(des_vel, des_pt)

            des_theta = self.solver.minimize(self.des_pt)

            # cur_omega = solver.query_speed(des_vel, des_pt-des_vel*dt)
            des_omega = self.solver.query_speed(
                self.des_vel, [self.curSwivel, self.curUpdown, self.curElbow]
            )
            if des_omega is not None:
                tmp_omega = des_omega.copy()
                des_omega = (self.last_omega + np.array(des_omega)) / 2
                self.last_omega = tmp_omega

            if des_theta is not None and des_omega is not None:
                msg.data[0] = des_theta[0]
                msg.data[1] = -des_theta[1]  # warn
                msg.data[2] = des_theta[2]

                msg.data[3] = des_omega[0]
                msg.data[4] = -des_omega[1]  # warn
                msg.data[5] = des_omega[2]

                msg.data[6] = self.des_hand[0]
                msg.data[7] = self.des_hand[1]
                msg.data[8] = self.des_hand[2]
                # print(vel_msg)

            try:
                self.ang_pub.publish(msg)
            except rospy.ROSInterruptException:
                pass
            self.rate.sleep()


if __name__ == "__main__":
    wrapper = Inverse_kin_wrapper()
    wrapper.send()
