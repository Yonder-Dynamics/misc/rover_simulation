#! /usr/bin/env python
"""
Simulates yolo using the ground truth in the gazebo environment. Only cares about the x position and the size of the bounding box,
since as of now y doesn't matter. Will generate a bounding box with size based on the distance of the post from the rover, and x
position based on orientation. Will also generate a depth map that is uniform to all be the distance of the post from the rover,
as the obs -> binned obs only cares about the distance at the spot of the observation, for now.
"""
from nav_msgs.msg import Odometry
from std_msgs.msg import Header
from gazebo_msgs.srv import GetModelState, GetModelStateRequest
from darknet_ros_msgs.msg import BoundingBox, BoundingBoxes
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import rospy
import sys
import math
import numpy as np

rospy.init_node("yolo_sim")

rospy.wait_for_service("/gazebo/get_model_state")
get_model_srv = rospy.ServiceProxy("/gazebo/get_model_state", GetModelState)

depth_img_pub = rospy.Publisher('/zed/zed_node/depth/depth_registered', Image)
ar_bboxes_pub = rospy.Publisher('/armodel/bboxes', BoundingBoxes, queue_size=1)

odom = Odometry()
header = Header()
header.frame_id = "/odom"

post_model = GetModelStateRequest()
post_model.model_name = "post"

rover_model = GetModelStateRequest()
rover_model.model_name = "robot"

bridge = CvBridge()
zed_dimensions = (1080, 1920)
intrinsic = np.array(
    [[1, 0, zed_dimensions[1]/2],
     [0, 1, zed_dimensions[0]/2],
     [0, 0, 1]]
).astype(float)
yolo_delay = .2

r = rospy.Rate(3)

def dist(a, b):
    return math.sqrt((a.x - b.x) ** 2 +
                     (a.y - b.y) ** 2 +
                     (a.z - b.z) ** 2)

def send_bbox(bbox_msg):
    def send(_):
        ar_bboxes_pub.publish(bbox_msg)
    return send

def quaternion_rotation_matrix(Q):
    """
    Covert a quaternion into a full three-dimensional rotation matrix.
 
    Input
    :param Q: A 4 element array representing the quaternion (q0,q1,q2,q3) 
 
    Output
    :return: A 3x3 element matrix representing the full 3D rotation matrix. 
             This rotation matrix converts a point in the local reference 
             frame to a point in the global reference frame.
    """
    # Extract the values from Q
    q0 = Q[0]
    q1 = Q[1]
    q2 = Q[2]
    q3 = Q[3]

    s = 1 / float(q0 ** 2 + q1 ** 2 + q2 ** 2 + q3 ** 2)
    print(s)

    # First row of the rotation matrix
    r00 = 1 - s * 2 * (q1 ** 2 + q2 ** 2)
    r01 = s * 2 * (q0 * q1 - q2 * q3)
    r02 = s * 2 * (q0 * q2 + q1 * q3)

    # Second row of the rotation matrix
    r10 = s * 2 * (q0 * q1 + q2 * q3)
    r11 = 1 - s * 2 * (q0 ** 2 + q2 ** 2)
    r12 = s * 2 * (q1 * q2 - q0 * q3)

    # Third row of the rotation matrix
    r20 = s * 2 * (q0 * q2 - q1 * q3)
    r21 = s * 2 * (q1 * q2 + q0 * q3)
    r22 = 1 - s * 2 * (q0 ** 2 + q1 ** 2)

    # 3x3 rotation matrix
    rot_matrix =[[r00, r01, r02],
               [r10, r11, r12],
               [r20, r21, r22]]

    return rot_matrix

fov_ratio = .8

while not rospy.is_shutdown():
    post = get_model_srv(post_model)
    rover = get_model_srv(rover_model)

    distance = dist(rover.pose.position, post.pose.position)
    depth_msg = bridge.cv2_to_imgmsg(np.full(zed_dimensions, distance).astype(np.float32), "32FC1")
    depth_msg.header.stamp = rospy.Time.now()

    rot = quaternion_rotation_matrix([rover.pose.orientation.x, rover.pose.orientation.y, rover.pose.orientation.z, rover.pose.orientation.w])
    extrinsic = np.hstack((np.array(rot).T, np.array([
        [-rover.pose.position.x],
        [-rover.pose.position.y],
        [-rover.pose.position.z]
    ]))).astype(float)
    post_pos = np.array([[post.pose.position.x], [post.pose.position.y], [post.pose.position.z], [1]]).astype(float)
    res = np.matmul(extrinsic, post_pos)
    x = res[0][0]
    y = res[1][0]
    p = -y / x / fov_ratio * zed_dimensions[1] + zed_dimensions[1] / 2

    if p >= 0 and p <= zed_dimensions[1]:
        depth_img_pub.publish(depth_msg)
        boxes = BoundingBoxes()
        boxes.header.stamp = depth_msg.header.stamp
        box = BoundingBox()
        size = .15 / x / fov_ratio * zed_dimensions[1]
        box.xmin = max(p - size, 0)
        box.xmax = min(p + size, zed_dimensions[1])
        box.ymin = zed_dimensions[0]/2 - size
        box.ymax = zed_dimensions[0]/2 + size
        # TODO: do something smarter lol
        box.probability = 1 if x < 8 else 1 / math.sqrt(x)
        boxes.bounding_boxes = [box]
        sys.stderr.write("sending" + str(boxes))
        rospy.Timer(rospy.Duration(yolo_delay), send_bbox(boxes), oneshot=True)

    r.sleep()

