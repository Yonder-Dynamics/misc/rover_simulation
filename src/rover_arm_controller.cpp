#include <ros/ros.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Float64MultiArray.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/JointState.h>
#include <iostream>
#include <vector>
#include <iomanip>
#include <cmath>
#include <chrono>
#include "pid_calc.hpp"

using namespace std;
using namespace std::chrono;
#define _USE_MATH_DEFINES

ros::Subscriber cmd_ang_sub;
ros::Subscriber encoder_sub;
ros::Subscriber consts_sub;

ros::Publisher swivel_pub,
    updown_pub,
    elbow_pub,
    wristUpDown_pub,
    wristRotate_pub,
    handPincherLeft_pub,
    handPincherRight_pub;

double upDownGravityConst = 7.5;
double elbowGravityConst = 8;

double swivelSpeedP = 10;
double swivelSpeedD = 0;

double upDownSpeedP = 30;
double upDownSpeedD = 0.5;

double elbowSpeedP = 20;
double elbowSpeedD = 0.5;

double swivel_kP = 300;
double swivel_kD = 50;

double upDown_kP = 350;
double upDown_kD = 8;

double elbow_kP = 300;
double elbow_kD = 7;

PID_Controller swivel_ctrl, swivelSpeedCtrl;
PID_Controller upDown_ctrl, upDownSpeedCtrl;
PID_Controller elbow_ctrl, elbowSpeedCtrl;

high_resolution_clock::time_point lastTime = high_resolution_clock::now();
double lastSwivel = 0, lastUpDown = 0, lastElbow = 0;
double curSwivel = 0, curUpDown = 0, curElbow = 0;
double swivelSpeed = 0, upDownSpeed = 0, elbowSpeed = 0;

void updatePose(const sensor_msgs::JointState states)
{
    curSwivel = states.position[9];
    curUpDown = states.position[10];
    curElbow = states.position[0];

    // swivelSpeed = states.velocity[7];
    // upDownSpeed = states.velocity[8];
    // elbowSpeed  = states.velocity[0]; // not accurate!

    high_resolution_clock::time_point curTime = high_resolution_clock::now();
    duration<double> time_span = curTime - lastTime;
    lastTime = curTime;
    double dt = time_span.count();
    swivelSpeed = (curSwivel - lastSwivel) / dt;
    upDownSpeed = (curUpDown - lastUpDown) / dt;
    elbowSpeed = (curElbow - lastElbow) / dt;

    lastSwivel = curSwivel;
    lastUpDown = curUpDown;
    lastElbow = curElbow;
}

void updateConsts(const std_msgs::Float64MultiArray constArr)
{
    upDownGravityConst = constArr.data[0];
    elbowGravityConst = constArr.data[1];
    swivelSpeedP = constArr.data[2];
    swivelSpeedD = constArr.data[3];
    upDownSpeedP = constArr.data[4];
    upDownSpeedD = constArr.data[5];
    elbowSpeedP = constArr.data[6];
    elbowSpeedD = constArr.data[7];
    swivel_kP = constArr.data[8];
    swivel_kD = constArr.data[9];
    upDown_kP = constArr.data[10];
    upDown_kD = constArr.data[11];
    elbow_kP = constArr.data[12];
    elbow_kD = constArr.data[13];

    swivelSpeedCtrl.setKs(swivelSpeedP, 0, swivelSpeedD);
    upDownSpeedCtrl.setKs(upDownSpeedP, 0, upDownSpeedD);
    elbowSpeedCtrl.setKs(elbowSpeedP, 0, elbowSpeedD);

    swivel_ctrl.setKs(swivel_kP, 0, swivel_kD);
    upDown_ctrl.setKs(upDown_kP, 0, upDown_kD);
    elbow_ctrl.setKs(elbow_kP, 0, elbow_kD);
}

double elbowGravity()
{
    double theta = curElbow + curUpDown;
    double l = cos(theta);
    double manipulatorMass = 1;
    return l; // the torque is proportinal to l assuming the end effector has no mass
}

double upDownGravity()
{
    double theta = -curUpDown;
    double l = cos(theta);
    double bicepMass = 1.28;
    double forearmMass = 3;
    return (l / 2) * bicepMass + (l + elbowGravity() / 2) * forearmMass; // torque from bicep+forearm
}

void callback(const std_msgs::Float64MultiArray msg)
{
    double desSwivel = msg.data[0];
    double desUpDown = msg.data[1];
    double desElbow = msg.data[2];
    double desSwivelSpeed = msg.data[3];
    double desUpDownSpeed = msg.data[4];
    double desElbowSpeed = msg.data[5];
    double desWristUpdown = msg.data[6];
    double desWristRotate = msg.data[7];
    double desHandPincher = msg.data[8];

    std_msgs::Float64 swivel, upDown, elbow, wristUpDown, wristRotate, handPincherLeft, handPincherRight;

    // swivel.data = swivelSpeedCtrl.getPID(desSwivelSpeed-swivelSpeed) + swivel_ctrl.getPID(desSwivel-curSwivel);
    // upDown.data = (-upDownGravity()*upDownGravityConst) + upDownSpeedCtrl.getPID(desUpDownSpeed-upDownSpeed) + upDown_ctrl.getPID(desUpDown-curUpDown);
    //  elbow.data = ( -elbowGravity()* elbowGravityConst) +  elbowSpeedCtrl.getPID(desElbowSpeed - elbowSpeed)  + elbow_ctrl.getPID(desElbow -curElbow);

    // this will be used in production
    // double swivelPID = swivel_ctrl.getPID(desSwivel-curSwivel);
    // double upDownPID = upDown_ctrl.getPID(desUpDown-curUpDown);
    // double elbowPID = elbow_ctrl.getPID(desElbow-curElbow);
    // swivel.data = desSwivelSpeed*swivelSpeedP + swivelPID;
    // upDown.data = (-upDownGravity()*upDownGravityConst) + desUpDownSpeed*upDownSpeedP + upDownPID;
    //  elbow.data = ( -elbowGravity()* elbowGravityConst) + desElbowSpeed *elbowSpeedP  +  elbowPID;

    // cout << fixed << setprecision(3);
    // cout << "desUpDown: " << desUpDown << " desUpDownSpeed: " << desUpDownSpeed << " upDownSpeed: " << upDownSpeed << endl;

    // pure position control
    swivel.data = desSwivel;
    upDown.data = desUpDown;
    elbow.data = desElbow;
    // cout <<  "curSwivel: " << curSwivel;
    // cout << " curUpDown: " << curUpDown << " offset: " << upDownGravity() << " const:" << upDownSpeedConst;
    // cout << " curElbow: "  << curElbow  << " offset: " <<  elbowGravity() << " const:" <<  elbowSpeedConst;
    // cout << endl;

    // pure speed control
    // swivel.data = desSwivelSpeed;
    // upDown.data = desUpDownSpeed + (-upDownGravity()*upDownGravityConst);
    //  elbow.data = desElbowSpeed  + ( -elbowGravity()* elbowGravityConst);
    //cout << swivel.data << " " << upDown.data << " " << elbow.data << endl;

    wristUpDown.data = desWristUpdown + (-curUpDown - curElbow); // set point plus offset
    wristRotate.data = desWristRotate;
    handPincherLeft.data = desHandPincher;
    handPincherRight.data = -desHandPincher;

    swivel_pub.publish(swivel);
    updown_pub.publish(upDown);
    elbow_pub.publish(elbow);
    wristUpDown_pub.publish(wristUpDown);
    wristRotate_pub.publish(wristRotate);
    handPincherLeft_pub.publish(handPincherLeft);
    handPincherRight_pub.publish(handPincherRight);
}

int main(int argc, char **argv)
{

    ros::init(argc, argv, "rover_arm_controller");
    ros::NodeHandle n;

    swivel_pub = n.advertise<std_msgs::Float64>("/arm/shoulder_swivel/command", 0);
    updown_pub = n.advertise<std_msgs::Float64>("/arm/shoulder_updown/command", 0);
    elbow_pub = n.advertise<std_msgs::Float64>("/arm/elbow/command", 0);
    wristUpDown_pub = n.advertise<std_msgs::Float64>("/arm/wrist_updown/command", 0);
    wristRotate_pub = n.advertise<std_msgs::Float64>("/arm/wrist_swivel/command", 0);
    handPincherLeft_pub = n.advertise<std_msgs::Float64>("/arm/pinchy_left/command", 0);
    handPincherRight_pub = n.advertise<std_msgs::Float64>("/arm/pinchy_right/command", 0);

    cmd_ang_sub = n.subscribe<std_msgs::Float64MultiArray>("/rover_simulation/cmd_ang", 1, &callback);

    encoder_sub = n.subscribe<sensor_msgs::JointState>("/joint_states", 1, &updatePose);

    swivel_ctrl.setCap(1000);
    upDown_ctrl.setCap(1000);
    elbow_ctrl.setCap(1000);
    swivel_ctrl.setICap(10);
    upDown_ctrl.setICap(10);
    elbow_ctrl.setICap(10);
    swivel_ctrl.setKs(swivel_kP, 0, swivel_kD);
    upDown_ctrl.setKs(upDown_kP, 0, upDown_kD);
    elbow_ctrl.setKs(elbow_kP, 0, elbow_kD);

    swivelSpeedCtrl.setCap(30);
    upDownSpeedCtrl.setCap(30);
    elbowSpeedCtrl.setCap(30);
    swivelSpeedCtrl.setICap(10);
    upDownSpeedCtrl.setICap(10);
    elbowSpeedCtrl.setICap(10);
    swivelSpeedCtrl.setKs(swivelSpeedP, 0, swivelSpeedD);
    upDownSpeedCtrl.setKs(upDownSpeedP, 0, upDownSpeedD);
    elbowSpeedCtrl.setKs(elbowSpeedP, 0, elbowSpeedD);

    consts_sub = n.subscribe<std_msgs::Float64MultiArray>("/rover_simulation/tuning_params", 1, &updateConsts);

    ros::spin();
}
