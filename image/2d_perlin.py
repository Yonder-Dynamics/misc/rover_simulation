import numpy as np
from PIL import Image
from sys import argv

seed = int(argv[1])

chunk_sizes = np.array([5, 7, 11, 17, 23, 51])
amplitude = [32, 24, 16, 8, 6, 4]

img_size = 257 # must be 2^n+1

noise_percent = 0.01

def noise(r, c, grad_field):
  r0 = int(r)
  r1 = r0 + 1
  c0 = int(c)
  c1 = c0 + 1

  center = np.array((r, c))
  pt0 = (r0, c0)
  pt1 = (r0, c1)
  pt2 = (r1, c0)
  pt3 = (r1, c1)

  vec0 = center-pt0
  vec1 = center-pt1
  vec2 = center-pt2
  vec3 = center-pt3

  grad0 = grad_field[pt0]
  grad1 = grad_field[pt1]
  grad2 = grad_field[pt2]
  grad3 = grad_field[pt3]

  prod0 = np.dot(grad0, vec0)
  prod1 = np.dot(grad1, vec1)
  prod2 = np.dot(grad2, vec2)
  prod3 = np.dot(grad3, vec3)

  c_weight = -2*(c-c0)**3 + 3*(c-c0)**2
  row1 = prod0 + c_weight*(prod1-prod0)
  row2 = prod2 + c_weight*(prod3-prod2)
  r_weight = -2*(r-r0)**3 + 3*(r-r0)**2
  val = row1 + r_weight*(row2-row1)

  return val

intervals = chunk_sizes/img_size
grid_sizes = chunk_sizes+1
np.random.seed(seed)

num_of_octaves = len(chunk_sizes)
print(num_of_octaves)

combined_img = np.zeros(shape=([img_size]*2))
for i in range(num_of_octaves):
  print(i)
  interval = intervals[i]
  grid_size = grid_sizes[i]
  grad_field = np.random.uniform(-1, 1, size = grid_size**2 * 2)

  grad_field = np.reshape(grad_field, (grid_size, grid_size, 2))

  # normalize
  for r in range(grid_size):
    for c in range(grid_size):
      grad_field[r][c] = grad_field[r][c]/np.linalg.norm(grad_field[r][c])
      rand_num = np.random.uniform()
      if rand_num >= 0.2: continue
      rand_sign = np.random.choice([-1, 1])
      grad_field[r][c] *= rand_sign
      if rand_num < 0.02:
        grad_field[r][c] *= amplitude[i]
      if rand_num < 0.025:
        grad_field[r][c] *= amplitude[i]/2
      elif rand_num < 0.05:
        grad_field[r][c] *= amplitude[i]/4
      elif rand_num < 0.1:
        grad_field[r][c] *= amplitude[i]/6
      elif rand_num < 0.2:
        grad_field[r][c] *= amplitude[i]/8

  octave = np.zeros_like(combined_img)
  for r in range(img_size):
    for c in range(img_size):
      octave[r][c] = noise(r*interval, c*interval, grad_field)
  octave = (octave+1)*amplitude[i]
  combined_img += octave
  print(np.min(octave), np.max(octave))

height_diff = min(255, np.max(combined_img) - np.min(combined_img))
print("height_diff", height_diff)
random_noise = np.random.uniform(-height_diff*noise_percent, height_diff*noise_percent, combined_img.shape)
combined_img += random_noise
#combined_img -= np.min(combined_img) # normalize to sea level

combined_img = combined_img.astype(np.uint8)
print(np.min(combined_img), np.max(combined_img))
img = Image.fromarray(combined_img)
img.save(str(seed)+"HeightMap.png")
