#include <ros/ros.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Int16.h>
#include <std_msgs/Float64MultiArray.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/Imu.h>
#include <iostream>
#include "quad_to_euler.hpp"
#include <vector>
#include "pid_calc.hpp"

using namespace std;

ros::Subscriber cmd_manual_vel_sub, cmd_autonomous_vel_sub, multiplexer;
ros::Publisher lf_pub, lm_pub, lb_pub, rf_pub, rm_pub, rb_pub;

ros::Subscriber imu;
float desAng = 0, curAng = 0;

int drive_system = 0;

PID_Controller pid_controller;

void changeDriveSystem(const std_msgs::Int16 msg)
{
    drive_system = msg.data;
}

void publish_commands(const std_msgs::Float64MultiArray msg)
{
    double left_vel, right_vel;
    left_vel = msg.data[0];
    right_vel = msg.data[1];

    std_msgs::Float64 left_command, right_command;
    left_command.data = -left_vel;
    right_command.data = right_vel;

    lf_pub.publish(left_command);
    lm_pub.publish(left_command);
    lb_pub.publish(left_command);
    rf_pub.publish(right_command);
    rm_pub.publish(right_command);
    rb_pub.publish(right_command);
}

void autonomous_command(const std_msgs::Float64MultiArray msg)
{
    if (drive_system == 0)
    {
        publish_commands(msg);
    }
}

void manual_command(const std_msgs::Float64MultiArray msg)
{
    if (drive_system == 1)
    {
        publish_commands(msg);
    }
}

int main(int argc, char **argv)
{

    ros::init(argc, argv, "rover_base_controller");
    ros::NodeHandle n;

    lf_pub = n.advertise<std_msgs::Float64>("/base/lf_controller/command", 0);
    lm_pub = n.advertise<std_msgs::Float64>("/base/lm_controller/command", 0);
    lb_pub = n.advertise<std_msgs::Float64>("/base/lb_controller/command", 0);
    rf_pub = n.advertise<std_msgs::Float64>("/base/rf_controller/command", 0);
    rm_pub = n.advertise<std_msgs::Float64>("/base/rm_controller/command", 0);
    rb_pub = n.advertise<std_msgs::Float64>("/base/rb_controller/command", 0);

    cmd_manual_vel_sub = n.subscribe<std_msgs::Float64MultiArray>("/manual_drive_train", 1, &manual_command);
    cmd_autonomous_vel_sub = n.subscribe<std_msgs::Float64MultiArray>("/autonomous_drive_train", 1, &autonomous_command);
    multiplexer = n.subscribe<std_msgs::Int16>("/drive_train_multiplexer", 1, &changeDriveSystem);

    ros::spin();
}
