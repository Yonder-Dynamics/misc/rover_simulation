#!/usr/bin/env python3

import rospy
from std_msgs.msg import Float64MultiArray
from sensor_msgs.msg import JointState
from tkinter import *
from solver import Inv_kin
import time
import math
import numpy as np

def clearSlider():
    x.set(0)
    y.set(0)
    z.set(0)

def clickEvent(e):
    clearSlider()

master = Tk()
x = Scale(master, from_=-0.5, to=0.5, label="x", resolution=0.001, length=200) # front back
x.bind("<Button-1>", clickEvent)
x.pack()

y = Scale(master, from_=-0.5, to=0.5, label="y", orient=HORIZONTAL, resolution=0.001, length=200) # left right
y.bind("<Button-1>", clickEvent)
y.pack()

z = Scale(master, from_=-0.5, to=0.5, label="z", resolution=0.001, length=200) # up down
z.bind("<Button-1>", clickEvent)
z.pack() # declare sliders

# x = Scale(master, from_=0, to=0.8, label="x", resolution=0.001, length=200) # front back
# x.set(0.3)
# x.pack()

# y = Scale(master, from_=-0.65, to=0.65, label="y", orient=tkinter.HORIZONTAL, resolution=0.001, length=200) # left right
# y.set(0)
# y.pack()

# z = Scale(master, from_=-0.3, to=0.8, label="z", resolution=0.001, length=200) # up down
# z.set(0)
# z.pack()

w1 = Scale(master, from_=-math.pi/2, to=math.pi/2, label="w1", resolution=0.001, length=200) # front back
w1.bind("<Button-1>", clickEvent)
w1.pack()

w2 = Scale(master, from_=-math.pi/2, to=math.pi/2, label="w2", orient=HORIZONTAL, resolution=0.001, length=200) # left right
w2.bind("<Button-1>", clickEvent)
w2.pack()


curSwivel, curUpdown, curElbow = [0.0]*3
def readAngles(msg):
    '''[elbow, lb, lf, lm, rb, rf, rm, shoulder_swivel, shoulder_updown, wrist1, wrist2, wrist3]'''
    global curSwivel
    global curUpdown
    global curElbow
    curSwivel = msg.position[7]
    curUpdown = -msg.position[8]
    curElbow = msg.position[0]
    #velocity = msg.velocity

def checkPointBound(des_pt):
    des_pt[0] = max(0, min(des_pt[0], 0.8))
    des_pt[1] = max(-0.65, min(des_pt[1], 0.65))
    des_pt[2] = max(-0.3, min(des_pt[2], 0.8))

def checkAngleBound(des_ang):
    if des_ang is None: return
    des_ang[0] = max(-1, min(des_ang[0], 1))
    des_ang[1] = max(0.4, min(des_ang[1], 2))
    des_ang[2] = max(0.1, min(des_ang[2], 2.35))

def checkOmegaBound(des_omg):
    if des_omg is None: return
    des_omg[0] = max(-0.3, min(des_omg[0], 0.3))
    des_omg[1] = max(-0.3, min(des_omg[1], 0.3))
    des_omg[2] = max(-0.3, min(des_omg[2], 0.3))

def send():
    pub = rospy.Publisher("/rover_simulation/cmd_ang", Float64MultiArray, queue_size=10)

    msg = Float64MultiArray()
    msg.data = [0]*8
    inv_kin = Inv_kin()

    clearSlider()
    des_pt = np.array([0.3, 0.0, 0.0])
    last_time = time.time()
    des_omega = np.array([0]*3)
    while not rospy.is_shutdown():
        master.update()

        if abs(x.get()) < 0.005: x.set(0)
        if abs(y.get()) < 0.005: y.set(0)
        if abs(z.get()) < 0.005: z.set(0) # patchy code for slider deadzone

        des_vel = np.array([x.get(), y.get(), z.get()]) # get desired speed

        cur_time = time.time()
        dt = cur_time-last_time # calculate delta time
        last_time = cur_time

        des_pt += des_vel*dt # update target point
        checkPointBound(des_pt) # bound the value in a certain range
        # print(des_vel, des_pt)

        des_theta = inv_kin.minimize(des_pt)
        checkAngleBound(des_theta)
        # cur_omega = inv_kin.query_speed(des_vel, des_pt-des_vel*dt)
        cur_omega = inv_kin.query_speed(des_vel, [curSwivel, curUpdown, curElbow])
        checkOmegaBound(cur_omega)
        if cur_omega is not None:
            des_omega = (des_omega+np.array(cur_omega))/2
        #   print(des_omega)
        if des_theta is not None and des_omega is not None:
            msg.data[0] = des_theta[0]
            msg.data[1] = -des_theta[1] # warn
            msg.data[2] = des_theta[2]

            msg.data[3] = des_omega[0]
            msg.data[4] = -des_omega[1] # warn
            msg.data[5] = des_omega[2]

            msg.data[6] = w1.get()
            msg.data[7] = w2.get()
            #print(vel_msg)

        pub.publish(msg)
        rate.sleep()

if __name__ == '__main__':
    #print("press q to quit")
    rospy.init_node('control_sender', anonymous=True)
    rate = rospy.Rate(100) # 100hz
    joint_pos_sub = rospy.Subscriber("/joint_states", JointState, readAngles)
    try:
        send()
    except rospy.ROSInterruptException:
        pass
