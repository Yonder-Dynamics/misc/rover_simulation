#!/usr/bin/env python3

import rospy
from std_msgs.msg import Float64MultiArray
from controller import Controller

def send():
    pub = rospy.Publisher("/manual_drive_train", Float64MultiArray, queue_size=10)
    controller = Controller(speed_gain=20, omega_gain=20, speed_cap=100, omega_cap=50) # make a new controller
    controller.start() # start the controller in another thread
    vel_msg = Float64MultiArray()
    vel_msg.data = [0] * 2

    while not rospy.is_shutdown() and not controller.is_done():
        vel_msg.data[0] = controller.get_speed() + controller.get_omega()
        vel_msg.data[1] = controller.get_speed() - controller.get_omega()

        pub.publish(vel_msg)
        rate.sleep()

if __name__ == '__main__':
    #print("press q to quit")
    rospy.init_node('control_sender', anonymous=True)
    rate = rospy.Rate(50) # 50hz
    try:
        send()
    except rospy.ROSInterruptException:
        pass