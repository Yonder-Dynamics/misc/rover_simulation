# rover_simulation

This can be used by itself, or with moveit to control the arm

For use with moveit you need to install [moveit](https://gitlab.com/Yonder-Dynamics/controls/moveit) and run it, then you can send commands to the arm and control it.

To use randomly generated terrain, copy one of the heightmaps in ./image/maps/ to ./gazebo/models/dessert5/image/
Make sure to change the name to HeightMap.png

## Installiation instructions (alternatively use the installer located [here](https://gitlab.com/Yonder-Dynamics/installer))

1. Follow the ROS tutorial and install ROS melodic <http://wiki.ros.org/melodic/Installation.>
  *Be sure to install the "ros-melodic-desktop-full" version, since gazebo is needed for simulation
  
2. Now you should have created "\~/catkin_ws/". Clone this repo to "\~/catkin_ws/src/".

3. Make a folder for the models

```
  mkdir -p ~/.gazebo/models
```

4. Move the folder "./.models/desert5" to "~/.gazebo/models/"

```
  cp -r ./.models/desert5 ~/.gazebo/models/
```

5. Install dependencies for gazebo to communicate with ROS

```
  sudo apt install ros-melodic-gazebo-ros-control ros-melodic-effort-controllers
```

6. [Optional] Install [Eigen](http://eigen.tuxfamily.org/) with the instruction in the file INSTALL. Eigen3 will cause some import issue with ROS. Fix it by duplicating the Eigen lib
```
  sudo cp -r /usr/local/include/eigen3/Eigen/ /usr/local/include/
```

7. Install rospy for python3, tkinter, and scipy
```
  sudo apt install python3-catkin-pkg-modules python3-rospkg-modules python3-tk
```
and 
```  
  pip3 install scipy
```

8. run catkin_make

```
  cd ~/catkin_ws && catkin_make
```

9. Source the setup.sh file to add the project folder to ros.

```
  source ~/catkin_ws/devel/setup.sh
```

10. Run the simulation demo, control rover movement by arrow keys (when you have terminal window selected).

```
  roslaunch rover_simulation start.launch
```

## Sensors

### 1. Depth camera

  topic:

  ```
  /rover_simulation/body_camera/
  raw image: /rover_simulation/body_camera/color/image_raw
  point cloud: /rover_simulation/body_camera/depth/points
  ```

### 2. Ground camera

  topic:

  ```
  /rover_simulation/ground_camera
  raw image: /rover_simulation/ground_camera/image_raw
  ```

### 3. imu

  topic:

  ```
  /rover_simulation/imu
  ```

### 4. GPS

  topic:

  ```
  /rover_simulation/gps
  position: /rover_simulation/gps/position
  velocity: /rover_simulation/gps/velocity
  ```

### 5. Ultrasonic Senros

  topic:

  ```
  /rover_simulation/ultrasonic_0 is the far left
  /rover_simulation/ultrasonic_4 is the far right
  ```
